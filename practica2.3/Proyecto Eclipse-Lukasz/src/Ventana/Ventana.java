package Ventana;

import javax.swing.*;
/**
 * 
 * @author Lukasz Kaczynski
 * @since 15/01/2018
 *
 */
public class Ventana extends  JFrame{

	private static final long serialVersionUID = 3360524391047328045L;
	Ventana() {
        initComponents();
    }
	private void initComponents() {

        jScrollPane1 = new  JScrollPane();
        jPanel1 = new  JPanel();
        jPanel2 = new  JPanel();
        jLabel6 = new  JLabel();
        jRadioButton1 = new  JRadioButton();
        jSpinner1 = new  JSpinner();
        jRadioButton2 = new  JRadioButton();
        jLabel7 = new  JLabel();
        jLabel1 = new  JLabel();
        jScrollPane2 = new  JScrollPane();
        jTextArea1 = new  JTextArea();
        jLabel2 = new  JLabel();
        jLabel5 = new  JLabel();
        jLabel3 = new  JLabel();
        jComboBox1 = new  JComboBox<>();
        jLabel4 = new  JLabel();
        jTextField1 = new  JTextField();
        jCheckBox1 = new  JCheckBox();
        jTextField2 = new  JTextField();
        jSlider1 = new  JSlider();
        jButton1 = new  JButton();
        jTabbedPane1 = new  JTabbedPane();
        jPanel3 = new  JPanel();
        jLabel9 = new  JLabel();
        jPanel4 = new  JPanel();
        jLabel8 = new  JLabel();
        jMenuBar1 = new  JMenuBar();
        jMenu1 = new  JMenu();
        jMenu2 = new  JMenu();
        jMenu3 = new  JMenu();
        jMenuItem1 = new  JMenuItem();
        jMenuItem2 = new  JMenuItem();

        setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ventana Eclipse");

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        jLabel6.setHorizontalAlignment( SwingConstants.RIGHT);
        jLabel6.setText("Habilidad:");

        jRadioButton1.setText("Homnbre");
        

        jRadioButton2.setText("Mujer");

        jLabel7.setHorizontalAlignment( SwingConstants.RIGHT);
        jLabel7.setText("Tonteria:");

        jLabel1.setHorizontalAlignment( SwingConstants.RIGHT);
        jLabel1.setText(" Usuario:");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jLabel2.setHorizontalAlignment( SwingConstants.RIGHT);
        jLabel2.setText("Nombre:");

        jLabel5.setHorizontalAlignment( SwingConstants.RIGHT);
        jLabel5.setText("Opcion:");

        jLabel3.setHorizontalAlignment( SwingConstants.RIGHT);
        jLabel3.setText(" Sexo:");

        jComboBox1.setModel(new  DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel4.setHorizontalAlignment( SwingConstants.RIGHT);
        jLabel4.setText("Descripcion:");

        jTextField1.setText("jTextField1");

        jCheckBox1.setText("Valido");

        jTextField2.setText("jTextField1");

        jButton1.setText("A�adir");

         GroupLayout jPanel2Layout = new  GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jButton1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6,  GroupLayout.PREFERRED_SIZE, 59,  GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7,  GroupLayout.PREFERRED_SIZE, 59,  GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap( LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
                            .addComponent(jSlider1,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinner1,  GroupLayout.PREFERRED_SIZE, 192,  GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1,  GroupLayout.PREFERRED_SIZE, 59,  GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1,  GroupLayout.PREFERRED_SIZE, 89,  GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2,  GroupLayout.PREFERRED_SIZE, 59,  GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2,  GroupLayout.PREFERRED_SIZE, 89,  GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3,  GroupLayout.PREFERRED_SIZE, 59,  GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap( LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jRadioButton1)
                        .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jRadioButton2))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4,  GroupLayout.PREFERRED_SIZE, 59,  GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2,  GroupLayout.PREFERRED_SIZE, 200,  GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5,  GroupLayout.PREFERRED_SIZE, 59,  GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap( LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBox1)
                            .addComponent(jComboBox1,  GroupLayout.PREFERRED_SIZE, 194,  GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE))
                .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE))
                .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2))
                .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jScrollPane2,  GroupLayout.PREFERRED_SIZE, 115,  GroupLayout.PREFERRED_SIZE))
                .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jComboBox1,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE))
                .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1)
                .addPreferredGap( LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel6,  GroupLayout.DEFAULT_SIZE,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSlider1,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE))
                .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jSpinner1,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7,  GroupLayout.DEFAULT_SIZE,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(5, 5, 5)
                .addComponent(jButton1))
        );

        jPanel3.setBackground(new java.awt.Color(153, 153, 153));

        jLabel9.setBackground(new java.awt.Color(102, 102, 102));
        jLabel9.setIcon(new  ImageIcon(getClass().getResource("/Images/hello-speech-bubble-handmade-chatting-symbol.png"))); 

         GroupLayout jPanel3Layout = new  GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9,  GroupLayout.DEFAULT_SIZE,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9,  GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Pesta�a 1", jPanel3);

        jLabel8.setFont(new java.awt.Font("Tekton Pro", 1, 24)); // NOI18N
        jLabel8.setHorizontalAlignment( SwingConstants.CENTER);
        jLabel8.setText("Hola");

         GroupLayout jPanel4Layout = new  GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8,  GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8,  GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Pesta�a 2", jPanel4);

         GroupLayout jPanel1Layout = new  GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTabbedPane1,  GroupLayout.PREFERRED_SIZE, 331,  GroupLayout.PREFERRED_SIZE)
                .addContainerGap(93, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTabbedPane1)
                    .addComponent(jPanel2,  GroupLayout.DEFAULT_SIZE,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(204, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel1);

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        jMenu3.setText("Herrramientas");

        jMenuItem1.setText("sub1");
        jMenu3.add(jMenuItem1);

        jMenuItem2.setText("sub2");
        jMenu3.add(jMenuItem2);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

         GroupLayout layout = new  GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1,  GroupLayout.PREFERRED_SIZE, 751,  GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1,  GroupLayout.PREFERRED_SIZE, 554,  GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
	
	}
	
	
	
	
	private  JButton jButton1;
    private  JCheckBox jCheckBox1;
    private  JComboBox<String> jComboBox1;
    private  JLabel jLabel1;
    private  JLabel jLabel2;
    private  JLabel jLabel3;
    private  JLabel jLabel4;
    private  JLabel jLabel5;
    private  JLabel jLabel6;
    private  JLabel jLabel7;
    private  JLabel jLabel8;
    private  JLabel jLabel9;
    private  JMenu jMenu1;
    private  JMenu jMenu2;
    private  JMenu jMenu3;
    private  JMenuBar jMenuBar1;
    private  JMenuItem jMenuItem1;
    private  JMenuItem jMenuItem2;
    private  JPanel jPanel1;
    private  JPanel jPanel2;
    private  JPanel jPanel3;
    private  JPanel jPanel4;
    private  JRadioButton jRadioButton1;
    private  JRadioButton jRadioButton2;
    private  JScrollPane jScrollPane1;
    private  JScrollPane jScrollPane2;
    private  JSlider jSlider1;
    private  JSpinner jSpinner1;
    private  JTabbedPane jTabbedPane1;
    private  JTextArea jTextArea1;
    private  JTextField jTextField1;
    private  JTextField jTextField2;
}
