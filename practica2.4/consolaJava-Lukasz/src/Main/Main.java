package Main;

import java.util.Scanner;

public class Main {
	static Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		menu();
	}
	public static void menu(){
		System.out.println("Seleccione una opcion:");
		System.out.println("1.\tSumar");
		System.out.println("2.\tRestar");
		System.out.println("3.\tMultiplicar");
		System.out.println("4.\tSalir");
		int i=0;
		
		do{
			i=getNum("");
			if(i<1||i>4){
				i=0;
				System.out.println("Por favor selecione una opcion valida.");
			}
		}while(i==0);
			
		switch(i){
		case 1:
			suma();
			break;
		case 2:
			resta();
			break;
		case 3:
			multi();
			break ;
		case 4:
			salir();
			break;
			
		}
		System.out.println("Presiona Enter para continuar...");
		in.hasNextLine();
		menu();
	}
	public static void suma(){
		int i=getNum("Introduce primer numero\n");
		int e=getNum("Introduce segundo numero\n");
		int sum=e+i;
		System.out.println("resultado de: "+i+"+"+e+"="+sum);
	}
	public static void resta(){
		int i=getNum("Introduce primer numero\n");
		int e=getNum("Introduce segundo numero\n");
		int sum=e-i;
		System.out.println("resultado de: "+i+"-"+e+"="+sum);
	}
	public static void multi(){
		int i=getNum("Introduce primer numero\n");
		int e=getNum("Introduce segundo numero\n");
		int sum=e*i;
		System.out.println("resultado de: "+i+"*"+e+"="+sum);
	}
	public static void salir(){
		System.exit(0);
	}
	public static int getNum(String g){
		int i=0;
		System.out.print(g);
		do{
			System.out.print("-> ");
			if(in.hasNextInt()){
				i=in.nextInt();
				in.nextLine();
			}else{
				in.nextLine();
				System.out.println("Por favor selecione una opcion valida.");
			}
		
		}while(i==0);
		return i;
	}
}
