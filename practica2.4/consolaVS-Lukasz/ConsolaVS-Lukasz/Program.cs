﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVS_Lukasz
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }
        public static void Menu()
        {
            Console.WriteLine("Seleccione una opcion:");
            Console.WriteLine("1.\tSumar");
            Console.WriteLine("2.\tRestar");
            Console.WriteLine("3.\tMultiplicar");
            Console.WriteLine("4.\tSalir");
            int i = 0;

            do
            {
                i = GetNum("");
                if (i < 1 || i > 4)
                {
                    i = 0;
                    Console.WriteLine("Por favor selecione una opcion valida.");
                }
            } while (i == 0);

            switch (i)
            {
                case 1:
                    Suma();
                    break;
                case 2:
                    Resta();
                    break;
                case 3:
                    Multi();
                    break;
                case 4:
                    Salir();
                    break;

            }
            Console.WriteLine("Presiona Enter para continuar...");
            Console.ReadKey();
            Menu();
        }

        private static void Print(string v)
        {
            throw new NotImplementedException();
        }

        public static void Suma()
        {
            int i = GetNum("Introduce primer numero\n");
            int e = GetNum("Introduce segundo numero\n");
            int sum = e + i;
            Console.WriteLine("resultado de: " + i + "+" + e + "=" + sum);
        }
        public static void Resta()
        {
            int i = GetNum("Introduce primer numero\n");
            int e = GetNum("Introduce segundo numero\n");
            int sum = e - i;
            Console.WriteLine("resultado de: " + i + "-" + e + "=" + sum);
        }
        public static void Multi()
        {
            int i = GetNum("Introduce primer numero\n");
            int e = GetNum("Introduce segundo numero\n");
            int sum = e * i;
            Console.WriteLine("resultado de: " + i + "*" + e + "=" + sum);
        }
        public static void Salir()
        {
            Environment.Exit(0);
        }
        public static int GetNum(String g)
        {
            int i = 0;
            Console.Write(g);
            do
            {
                Console.Write("-> ");
                String s=Console.ReadLine();
                try
                {
                    i = Int32.Parse(s);
                }
                catch (FormatException e) {
                    Console.WriteLine(e.Message);
                }
                

            } while (i == 0);
            return i;
        }
    }
}
