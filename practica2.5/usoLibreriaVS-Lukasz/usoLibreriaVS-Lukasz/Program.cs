﻿using LibreriaVS_Lukasz;
using System;

namespace usoLibreriaVS_Lukasz
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
		 * Class1
		 */
            if (Class1.EsPerfecto(8128)) { Console.WriteLine("es perfecto"); }
            if (Class1.EsPrimo(23)) { Console.WriteLine("es primo"); }
            Class1.MostrarHola();
            Class1.Potencias(2, 32);
            Console.WriteLine(Class1.ReadInt("Introduce numero"));

            /**
             * Class2
             */
            Class2.Fecha1 = DateTime.Now;
            Class2.MostrarAnio();
            Class2.MostrarDia();
            Class2.MostrarHora();
            Class2.MostrarMes();
            Class2.MostrarMinuto();
            Console.ReadKey();
        }
    }
}
