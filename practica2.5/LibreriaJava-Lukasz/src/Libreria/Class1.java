package Libreria;
import java.util.ArrayList;
import java.util.Scanner;


public class Class1 {
	static Scanner in = new Scanner(System.in);
	  public static int readInt(String g) {
          int i = 0;
          System.out.print(g);
          
          do{
  			System.out.print("-> ");
  			if(in.hasNextInt()){
  				i=in.nextInt();
  				in.nextLine();
  			}else{
  				in.nextLine();
  				System.out.println("Por favor selecione una opcion valida.");
  			}
  		
  		}while(i==0);
          return i;
      }

      public static boolean esPerfecto(int numero) {
    	  int suma=0;
    	  ArrayList<Integer> t=factores(numero);
    	  for(int i = 0; i<t.size(); i++){
    		  suma+=t.get(i);
    	  }
    	  return numero == suma;
      }

      public static void potencias(int from, int to)
      {
          for (int potencia = 0; potencia <= to; potencia++)
              System.out.println(from+"^"+potencia+" = "+Math.pow(from, potencia));
      }

      public static boolean esPrimo(int n) {
          int a = 0, i;
          for (i = 1; i < (n + 1); i++)
          {
              if (n % i == 0)
              {
                  a++;
              }
          }
          if (a != 2)
          {
              return false;
          }
          else
          {
              return true;
          }
         
      }

      public static void mostrarHola() {
          System.out.println("Hola");
      }

      private static ArrayList<Integer> factores(int numero)
      {
          ArrayList<Integer> factores = new ArrayList<Integer>();
          for (int i = 1; i < numero; i++)
              if (numero % i == 0) factores.add(i);
          return factores;
      }
     
}
