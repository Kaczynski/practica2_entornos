package Libreria;

import java.util.Calendar;

public class Class2 {
	 
	static Calendar cal = Calendar.getInstance();
     public static void mostrarDia()
     {
         System.out.println("Dia: " +  cal.get(Calendar.DAY_OF_MONTH));

     }

     public static void mostrarMes()
     {
         System.out.println("Mes: " + cal.get(Calendar.MONTH));
     }

     public static void mostrarHora()
     {
         System.out.println("Hora: " + cal.get(Calendar.HOUR));
     }

     public static void mostrarMinuto()
     {
         System.out.println("Minuto: " + cal.get(Calendar.MINUTE));
     }

     public static void mostrarAnio() {
         System.out.println("A�o: " + cal.get(Calendar.YEAR));
     }
}
