import Libreria.Class1;
import Libreria.Class2;

public class Main {

	public static void main(String[] args) {
		/*
		 * Class1
		 */
		if(Class1.esPerfecto(8128)){System.out.println("es perfecto");}
		if(Class1.esPrimo(23)){System.out.println("es primo");}
		Class1.mostrarHola();
		Class1.potencias(2, 32);
		System.out.println(Class1.readInt("Introduce numero"));
		
		/**
		 * Class2
		 */
		Class2.mostrarAnio();
		Class2.mostrarDia();
		Class2.mostrarHora();
		Class2.mostrarMes();
		Class2.mostrarMinuto();
	}

}
