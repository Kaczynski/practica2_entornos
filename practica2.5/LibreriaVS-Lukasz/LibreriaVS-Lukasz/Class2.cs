﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_Lukasz
{
    
    public class Class2
    {
        static DateTime Fecha = new DateTime();

        public static DateTime Fecha1 { get => Fecha; set => Fecha = value; }

        public static void MostrarDia()
        {
            Console.WriteLine("Dia: " + Fecha.Day);

        }

        public static void MostrarMes()
        {
            Console.WriteLine("Mes: " + Fecha.Month);
        }

        public static void MostrarHora()
        {
            Console.WriteLine("Hora: " + Fecha.Hour);
        }

        public static void MostrarMinuto()
        {
            Console.WriteLine("Minuto: " + Fecha.Minute);
        }

        public static void MostrarAnio() {
            Console.WriteLine("Año: " + Fecha.Year);
        }
    }
}
