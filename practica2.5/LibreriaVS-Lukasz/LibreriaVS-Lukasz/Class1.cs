﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_Lukasz
{
    public class Class1
    {
        public static int ReadInt(String g) {
            int i = 0;
            Console.Write(g);
            do
            {
                Console.Write("-> ");
                String s = Console.ReadLine();
                try
                {
                    i = Int32.Parse(s);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            } while (i == 0);
            return i;
        }

        public static bool EsPerfecto(int numero) {
            return numero == Factores(numero).Sum();
        }

        public static void Potencias(int from, int to)
        {
            for (int potencia = 0; potencia <= to; potencia++)
                Console.WriteLine("{0}^{1} = {2:N0} (0x{2:X})", from, potencia, (long)Math.Pow(from, potencia));
        }

        public static bool EsPrimo(int n) {
            int a = 0, i;
            for (i = 1; i < (n + 1); i++)
            {
                if (n % i == 0)
                {
                    a++;
                }
            }
            if (a != 2)
            {
                return false;
            }
            else
            {
                return true;
            }
           
        }

        public static void MostrarHola() {
            Console.WriteLine("Hola");
        }

        private static List<int> Factores(int numero)
        {
            List<int> factores = new List<int>();
            for (int i = 1; i < numero; i++)
                if (numero % i == 0) factores.Add(i);
            return factores;
        }
       
    }
}
